# -*- coding:utf-8 -*-

"""
This file is part of ZLZ.

ZLZ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ZLZ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ZLZ.  If not, see <https://www.gnu.org/licenses/>
"""


"""
Handles importing drawables (plDrawableSpans and clusters).
Also handles building armatures for rigged meshes.
#"""

import bpy, bmesh
import PyHSPlasma as pl
from mathutils import *
from zero_length_zenith.sceneImporter import SceneImporter
from zero_length_zenith.utils import *
from korman.exporter.mesh import _RenderLevel

class DrawImporter:

    def __init__(self, parent):
        self.parent = parent
        self.bonesToReconstruct = {} # sceneObjKey: (drawableKey, transformIds)
        self.objectsUsingBones = {} # Blender object: (dspan, bone length)
        self.boneToArmature = {} # bone plKey: Blender armature (object, not data)
        self.bonesLength = .1 # display length of bones. Does not affect how the armature animates.

    def findArmatureSOK(self, boneKey):
        """Returns (False, the first sceneobject in the parent chain which is not a bone),
        or (True, the last sceneobject in the parent chain which was a bone) if there is no non-bone object higher in the parent chain."""
        parent = boneKey
        while True:
            oldParent = parent
            parent = self.parent.getParent(parent)
            if not parent or not self.parent.validKey(parent):
                # no parent - this bone simply exists at the root of the scene.
                # Return the last found bone instead
                return (True, oldParent)
            # Korman exports an intermediary object, ignore it.
            parent_is_korman_rest = parent.name.endswith("_REST")
            if not parent_is_korman_rest and not (self.parent.validKey(parent.object.draw) and self.getBoneData(None, parent.object.draw)):
                # no bone data found = we've come to the top of the bone hierarchy. Finally !
                return (False, parent)

    def getBoneData(self, sceneObjKey, drawKey):
        """Returns basic bone data: sceneObjKey, drawableKey, transformIds; or None if not a bone"""
        if not self.parent.validKey(drawKey):
            return None

        draw = drawKey.object
        if not isinstance(draw, pl.plDrawInterface): # this can happen for plInstanceDrawInterface
            print("WARNING: draw interface is of type %s, which is not supported. Skipping." % draw.__class__.__name__)
            return None

        # iterate over the submeshes
        for drawableKey, key in draw.drawables:
            if not self.parent.validKey(drawableKey):
                continue
            drawSpan = drawableKey.object
            if key == -1:
                # particle without visible data, ignore it
                continue
            if hasFlags(drawSpan.DIIndices[key].flags, pl.plDISpanIndex.kMatrixOnly):
                # this is a bone without visuals
                # note that one bone can have multiple transform ids - probably because they can be shared among meshes...
                return (sceneObjKey, drawableKey, drawSpan.DIIndices[key].indices)

        return None

    def importDraw(self, sceneObjKey, drawKey):
        if not drawKey or not drawKey.object:
            return (None, None)

        draw = drawKey.object
        if not isinstance(draw, pl.plDrawInterface): # this can happen for plInstanceDrawInterface
            print("WARNING: draw interface is of type %s, which is not supported. Skipping." % draw.__class__.__name__)
            return (None, None)

        boneData = self.getBoneData(sceneObjKey, drawKey)
        if boneData:
            # This is a bone. Bones generally don't contain visual data, although the draw interface contains the bone's position.
            # Return none - this will let the objectImporter generate an empty to which we'll attach all Korman modifiers.
            # However, we shall register this bone as needing to be imported later.
            self.bonesToReconstruct[boneData[0]] = boneData[1:]
            return (None, None)

        # visible object - importing...
        bm = bmesh.new() # "bmesh" is used to build and edit blender meshes
        msh = bpy.data.meshes.new(drawKey.name) # regular "mesh" that will be used once we're done editing the bmesh

        verts = []          # list of plvert
        allFaces = []       # TriangleWithMaterial
        originalFaces = {}  # bface: util.TriangleWithMaterial
        blUvLayers = []     # list of mesh uv layers
        blColorLayers = []  # list of mesh vertex color layers
        plMaterialKeys = {} # plasma material keys: pass index
        blMaterials = []    # blender materials used by the mesh
        numUvs = 0          # total number of uvs for this mesh
        hasWeights = False
        hasIndices = False
        bonesDSpan = None   # if hasWeights, key of dspan which contains the bones
        baseMatrix = 0
        numMatrices = 0
        sortFaces = False
        majorLevel = 0

        l2wMatrices = []
        localBoundingBoxes = []
        worldBoundingBoxes = []

        # iterate over the submeshes
        for drawableKey, key in draw.drawables:
            if not drawableKey or not drawableKey.object:
                continue
            drawSpan = drawableKey.object
            if key == -1:
                # particle without visible data, ignore it
                continue
            if hasFlags(drawSpan.criteria, pl.plDrawableSpans.kCritSortFaces):
                sortFaces = True
            majorLevel = max(majorLevel, drawSpan.renderLevel >> _RenderLevel._MAJOR_SHIFT)
            minorLevel = drawSpan.renderLevel & _RenderLevel._MINOR_MASK
            for icicleIndex in drawSpan.DIIndices[key].indices:
                if hasFlags(drawSpan.DIIndices[key].flags, pl.plDISpanIndex.kMatrixOnly):
                    # this is a bone without visuals - why are we processing it ?
                    raise RuntimeError("%s should be a visual mesh, not bone." % sceneObjKey.name)
                # now get the actual vertex and faces data
                icicle = drawSpan.spans[icicleIndex]
                l2w = icicle.localToWorld
                if l2w and not l2w.isIdentity():
                    l2wMatrices.append(Matrix(l2w.mat))
                else:
                    l2wMatrices.append(None)
                localBoundingBoxes.append(icicle.localBounds)
                worldBoundingBoxes.append(icicle.worldBounds)
                baseMatrix = icicle.baseMatrix
                numMatrices = max(numMatrices, icicle.numMatrices)
                bufferGroup = drawSpan.bufferGroups[icicle.groupIdx]
                numUvs = max(numUvs, bufferGroup.numUVs)
                if bufferGroup.skinWeights:
                    hasWeights = True
                    bonesDSpan = drawableKey
                    # some meshes can have weight... but no indices. Those are meshes deformed by two bones: the deform bone, and the world.
                    if bufferGroup.hasSkinIndices:
                        hasIndices = True
                plVertices = bufferGroup.getVertices(icicle.VBufferIdx) # reducing calls to this function might give a perf increase ?
                verts.extend(plVertices[icicle.VStartIdx : icicle.VStartIdx + icicle.VLength])
                plFaces = bufferGroup.getIndices(icicle.IBufferIdx, icicle.IStartIdx, icicle.ILength)
                plMat = drawSpan.materials[icicle.materialIdx]
                if plMat not in plMaterialKeys:
                    passIndex = minorLevel / 4 # dividing by 4 seems to be the standard
                    plMaterialKeys[plMat] = passIndex

                faceOffset = len(verts) - (icicle.VStartIdx + icicle.VLength)

                # store all faces along with their material id
                for i in range(icicle.ILength // 3):
                    allFaces.append(TriangleWithMaterial(
                        (
                            plFaces[i * 3]     + faceOffset,
                            plFaces[i * 3 + 1] + faceOffset,
                            plFaces[i * 3 + 2] + faceOffset,
                        ),
                        plMat
                    ))

        # fetch all required materials
        for mat, passIndex in plMaterialKeys.items():
            blMat = self.parent.matImporter.importMaterial(mat)
            blMat.pass_index = max(passIndex, blMat.pass_index)
            blMaterials.append(blMat)
            msh.materials.append(blMat)

        # import the existing (empty) mesh into the bmesh so it loads its materials
        bm.from_mesh(msh)
        blColorLayers.append(bm.loops.layers.color.new("Col"))
        blColorLayers.append(bm.loops.layers.color.new("Alpha"))
        # create all uv layers
        while len(blUvLayers) < numUvs:
            blUvLayers.append(bm.loops.layers.uv.new("UVMap%d"%len(blUvLayers)))
        # create deform layer if needed
        deformLayer = None
        if hasWeights:
            deformLayer = bm.verts.layers.deform.new()

        # create actual blender verts
        for vert in verts:
            bvert = bm.verts.new((vert.pos.X, vert.pos.Y, vert.pos.Z))
            if deformLayer:
                if hasIndices:
                    # BEWARE: importing Plasma rigging will only work if ALL submeshes share the EXACT same bone list !
                    boneIndex0 = (vert.skinIdx & 0xff)
                    boneIndex1 = (vert.skinIdx & 0xff << 8) >> 8
                    boneIndex2 = (vert.skinIdx & 0xff << 16) >> 16
                    if vert.skinWeights[0]: bvert[deformLayer][boneIndex0] = vert.skinWeights[0]
                    if vert.skinWeights[1]: bvert[deformLayer][boneIndex1] = vert.skinWeights[1]
                    if vert.skinWeights[2]: bvert[deformLayer][boneIndex2] = vert.skinWeights[2]
                else:
                    # THEORETICALLY, a mesh without indices is always deformed by a single bone and the world...
                    # ... so we only use the vertex' first weight
                    bvert[deformLayer][0] = vert.skinWeights[0] # weight is assigned to the null bone
                    bvert[deformLayer][1] = 1 - vert.skinWeights[0] # add remaining weight to the deform bone

        bm.verts.ensure_lookup_table()

        vcolUvLayerAR = None
        vcolUvLayerGB = None
        if self.parent.config["copyVcolToUv"]:
            vcolUvLayerAR = bm.loops.layers.uv.new("VCol_AR")
            vcolUvLayerGB = bm.loops.layers.uv.new("VCol_GB")

        # now create faces along with their color, uv, etc
        for face in allFaces:
            try:
                bface = bm.faces.new((
                    bm.verts[face.vertexIndices[0]],
                    bm.verts[face.vertexIndices[1]],
                    bm.verts[face.vertexIndices[2]],
                ))
                originalFaces[bface] = face
                bface.smooth = True # all faces are smooth, it's only the edges that are marked as sharp
                bface.material_index = blMaterials.index(self.parent.matImporter.knownMaterials[face.plMaterial])
                v0 = verts[face.vertexIndices[0]]
                v1 = verts[face.vertexIndices[1]]
                v2 = verts[face.vertexIndices[2]]
                v0uv = v0.UVWs
                v1uv = v1.UVWs
                v2uv = v2.UVWs
                for i in range(len(blUvLayers)):
                    bface.loops[0][blUvLayers[i]].uv = (v0uv[i].X, 1 - v0uv[i].Y) # 1-y for opengl
                    bface.loops[1][blUvLayers[i]].uv = (v1uv[i].X, 1 - v1uv[i].Y)
                    bface.loops[2][blUvLayers[i]].uv = (v2uv[i].X, 1 - v2uv[i].Y)
                # import colors
                # just like ol' PyPRP, we import the alpha channel into a separate color layer
                # it doesn't render differently, but allows the user to have a look at it
                v0col = getColorArrayFromInt(v0.color)
                v1col = getColorArrayFromInt(v1.color)
                v2col = getColorArrayFromInt(v2.color)
                bface.loops[0][blColorLayers[0]] = (*v0col[:3], 1)
                bface.loops[0][blColorLayers[1]] = (v0col[3], v0col[3], v0col[3], 1)
                bface.loops[1][blColorLayers[0]] = (*v1col[:3], 1)
                bface.loops[1][blColorLayers[1]] = (v1col[3], v1col[3], v1col[3], 1)
                bface.loops[2][blColorLayers[0]] = (*v2col[:3], 1)
                bface.loops[2][blColorLayers[1]] = (v2col[3], v2col[3], v2col[3], 1)
                if vcolUvLayerAR and vcolUvLayerGB:
                    # we also want vertex color to be stored in dedicated UV channels
                    bface.loops[0][vcolUvLayerAR].uv = (v0col[3], v0col[0])
                    bface.loops[0][vcolUvLayerGB].uv = v0col[1:3]
                    bface.loops[1][vcolUvLayerAR].uv = (v1col[3], v1col[0])
                    bface.loops[1][vcolUvLayerGB].uv = v1col[1:3]
                    bface.loops[2][vcolUvLayerAR].uv = (v2col[3], v2col[0])
                    bface.loops[2][vcolUvLayerGB].uv = v2col[1:3]
            except ValueError:
                # face already exists, so just ignore it
                pass

        recomputeNormals = False
        if self.parent.config["normalImportType"] == "RECOMPUTE":
            recomputeNormals = True

        if recomputeNormals:
            # Normals merge limit - if two normals are colinear by at least this factor, mark as smooth.
            # MOULa normals are compressed and therefore much less accurate. 0.9 should work for all file
            # formats, but with PotS we can crank up to .995 for more accuracy.
            normalAngleMergeLimit = .995 if drawKey.location.version == pl.pvPots else .9

            # now remove doubles to avoid duplicate verts
            # this means our mesh is now 100% smooth, but also that Plasma's mandatory duplicated vertices are finally gone...
            # note: merge distance MUST be .001. Blender has imprecisions with lower merge distance when far from
            # the origin, which destroys Gahreesen meshes.
            # (BTW, did anyone else notice the vertices of the turning bridges jump around due to floating point imprecision ?
            # Building anything at this distance is very optimistic if your engine doesn't support correct scene repositioning...)
            bmesh.ops.remove_doubles(bm, verts=bm.verts, dist=.001)
            bm.edges.ensure_lookup_table()

            # ...but smoothing the mesh means we lose the original normals.
            # We could add CustomSplitNormals to the mesh, but that would be ugly and impractical - the user
            # would no longer be able to modify the meshes. Also, some of Plasma's normals are compressed and point in a vague direction,
            # resulting in ugly lighting.
            # Instead, we mark some edges as sharp, based on the normals of the original vertices composing the edge. Finding the original
            # vertices is a bit cumbersome, but yields the BEST results - true to Cyan's original, without double vertices, without ugly compression,
            # and easy for the user to tweak. It's reasonably fast as well.
            # There are a few edge-case for which it won't work, like the vines in Gira - but those are bad meshes from the start and should be replaced anyway...
            for edge in bm.edges:
                if len(edge.link_faces) > 2:
                    # edge linked to more than two faces ? definitely rare and probably sharp
                    edge.smooth = False
                    continue
                if len(edge.link_faces) <= 1:
                    # border edge (1 face only), ignore it
                    edge.smooth = True
                    continue
                origFace0 = originalFaces[edge.link_faces[0]]
                origFace1 = originalFaces[edge.link_faces[1]]
                origVerts0 = [verts[origFace0.vertexIndices[i]] for i in range(3)]
                origVerts1 = [verts[origFace1.vertexIndices[i]] for i in range(3)]
                numSimilar = 0 # number of vertices with identical position and normals
                # iterate over all the vertices and compare them
                for vert0 in origVerts0:
                    for vert1 in origVerts1:
                        if vert0.pos.X == vert1.pos.X and vert0.pos.Y == vert1.pos.Y and vert0.pos.Z == vert1.pos.Z:
                            # those two vertices are the same (maybe different plVert instance, but same position)
                            if vert0.normal.dotP(vert1.normal) > normalAngleMergeLimit:
                                # normal angle is really small, this means these two vertices are indeed the same
                                numSimilar += 1
                            for uvIndex in range(len(vert0.UVWs)):
                                if vert0.UVWs[uvIndex].X != vert1.UVWs[uvIndex].X or vert0.UVWs[uvIndex].Y != vert1.UVWs[uvIndex].Y:
                                    edge.seam = True
                # if we have two identical pair of vertices, then this means the edge shared between those two faces is smooth
                # (if we have only one identical pair of vertices, we also sharpen the edge - Blender takes care of correctly smoothing the second vertex)
                edge.smooth = numSimilar >= 2

        if self.parent.config["importBounds"]:
            # draw the object's bounding boxes (one per material)
            for boundingBox in localBoundingBoxes:
                createBBoxVertices(bm, boundingBox)

            # World bounding boxes: since they ignore object transform, we'll just create a new object dedicated to those.
            worldBboxBm = bmesh.new()
            for boundingBox in worldBoundingBoxes:
                createBBoxVertices(worldBboxBm, boundingBox)
            worldBboxMesh = bpy.data.meshes.new(drawKey.name)
            worldBboxBm.to_mesh(worldBboxMesh)
            worldBboxBlObj = bpy.data.objects.new(sceneObjKey.name + "_WORLDBBOX", worldBboxMesh)
            self.parent.sceneImporter.appendObjectToScenes(worldBboxBlObj, SceneImporter.layerWorldBboxes)

        bm.normal_update() # generate normals, since we didn't supply them

        bm.to_mesh(msh) # and put it back into the final mesh !
        # use auto smooth so sharp edges change shading of object

        if recomputeNormals:
            msh.use_auto_smooth = True
            msh.auto_smooth_angle = 3.1415 # this disables angle-based smoothing, and only uses sharp edges

        # msh.validate() # updates the mesh's data to ensure normals are correct, etc. Shouldn't be required

        finalNumVerts = len(bm.verts)

        # make sure to cleanup the bmesh, so we don't uselessly lose memory
        bm.free()
        del bm

        if not finalNumVerts:
            # still no vertices ? Must be one of those pests particle emitters again... ignore the mesh.
            bpy.data.meshes.remove(msh)
            return (None, None)

        # NOTE: for rigging to work, we need to add vertex groups to the mesh
        # save a list of bone ids to add to the object later...
        blObj = bpy.data.objects.new(sceneObjKey.name + "_DRAW", msh)
        self.parent.sceneImporter.appendObjectToScenes(blObj, SceneImporter.layerDefault)
        if numMatrices:
            self.objectsUsingBones[blObj] = (bonesDSpan, baseMatrix, numMatrices)

        blObj.select = True
        bpy.context.scene.objects.active = blObj
        bpy.ops.object.mode_set(mode="EDIT")
        bpy.ops.mesh.sort_elements(type="MATERIAL") # may be better for some engines like Unity
        bpy.ops.object.mode_set(mode="OBJECT")
        blObj.select = False

        if len(draw.regions):
            visModifier = blObj.plasma_modifiers.visibility
            visModifier.enabled = True
            def setVisRegion(index, visRegBlObject):
                visModifier.regions[index].control_region = visRegBlObject
            for visRegionIndex, visRegionKey in enumerate(draw.regions):
                kormanRegion = visModifier.regions.add()
                self.parent.softVolumeImporter.registerVisRegionAndCallback(
                    visRegionKey,
                    lambda visRegBlObject, index=visRegionIndex: setVisRegion(index, visRegBlObject))

        for mat in msh.materials:
            if mat.use_transparency:
                blObj.show_transparent = True
                blObj.show_wire = True # juuuust in case... to make sure even completely transparent meshes can be seen
            t = self.parent.matImporter.lightmappedMaterials.get(mat)
            if t is not None:
                # is lightmapped - add a Korman lightmap modifier.
                if not blObj.plasma_modifiers.lightmap.enabled:
                    size, lightmapList, uvs = t
                    lightmapMod = blObj.plasma_modifiers.lightmap
                    lightmapMod.enabled = True
                    if size in (128, 256, 512, 1024, 2048):
                        lightmapMod.quality = str(size)
                    # Set the first lightmap as baked image.
                    # (Other lightmaps are either runtime-only, or one of my crazy hackeries.)
                    lightmapMod.image = lightmapList[0]
                    lightmapMod.uv_map = "UVMap%d" % uvs[0]
            if mat in self.parent.matImporter.doubleSidedMaterials:
                msh.show_double_sided = True

        if sortFaces or majorLevel in (_RenderLevel.MAJOR_FRAMEBUF, _RenderLevel.MAJOR_BLEND):
            blendMod = blObj.plasma_modifiers.blend
            blendMod.enabled = True
            if sortFaces:
                blendMod.sort_faces = "ALWAYS"
            if majorLevel == _RenderLevel.MAJOR_FRAMEBUF:
                blendMod.render_level = "FRAMEBUF"
            elif majorLevel == _RenderLevel.MAJOR_BLEND:
                blendMod.render_level = "BLEND"

        return (blObj, l2wMatrices[0])

    def rebuildArmatures(self):
        # Note that all Plasma bones are already mapped to a Blender bpy.types.Object - which is NOT a bone.
        # We'll rebuild an armature, which we'll insert in Blender's hierarchy. The sceneImporter will handle
        # reparenting objects to bones of the armatures which we will create.

        """
        NOTE: Plasma doesn't have the concept of "armature" root objects. Bones are free to be placed anywhere
        in the hierarchy - maybe even as root objects themselves.
        This screws up importing anything in Blender pretty bad (the FBX importer doesn't like it either from what I remember).

        We have the following solutions available:

        1. make one single armature for all rigged objects in the scene.
           That's what the FBX importer does, since it assumes FBX rarely contain multiple rigs (IIRC).
           Cons:
            - Not suitable for scenes with multiple rigs, which is our case.

        2. make one armature per drawable rigged object. Constrain each bone of this armature to follow
           the empty representing the original Plasma bone in the scene.
           Pros:
            - Keeps Plasma hierarchy.
            - Easier to import animations.
           Cons:
            - Authoring new animations would suck since you wouldn't animate the armature itself.
            - Can't be re-exported.

        3. attempt to group hierarchy branches under a single armature.
           Pros:
            - Can be re-exported.
            - Easy to create new anims.
            - Often represents armatures correctly.
           Cons:
            - Animations are harder to import (maybe ?)
            - Bones that are scene roots will each have their own armature

        We're obviously going with n.3. This is just so that you understand why importing Plasma armatures in Blender
        requires doing stupid shit.

        #"""

        knownArmatures = {} # sceneobject key: Blender object (of type armature)
        knownArmaturesDSpanAndBones = {} # blobj: dspan, [bone indices]
        boneNameToBoneKey = {}
        armatureBones = {} # Blender armature object: [bone plKeys,]
        willResetLocalTransformForArmatures = []

        # first, create all armatures
        for sceneObjKey in self.bonesToReconstruct:
            # We'll name/attach armatures as/to the first non-bone object upwards in the hierarchy chain.
            # (if there is none, name it after the first bone in the hierarchy instead.)
            isBone, armatureSOK = self.findArmatureSOK(sceneObjKey)
            arm = None
            armBlObj = knownArmatures.get(armatureSOK)
            if not armBlObj:
                # not already created. Do so now.
                arm = bpy.data.armatures.new(armatureSOK.name)
                armBlObj = bpy.data.objects.new(armatureSOK.name + "_ARMATURE", arm)
                armBlObj.show_x_ray = True
                arm.draw_type = "STICK"
                knownArmatures[armatureSOK] = armBlObj
                knownArmaturesDSpanAndBones[armBlObj] = (self.bonesToReconstruct[sceneObjKey][0], [])

                # link this armature to any scene containing the original object
                layers = [False,] * 20
                for id in SceneImporter.layerDefault:
                    layers[id] = True
                originalObject = self.parent.getBlObjectFromKey(armatureSOK)
                for scene in bpy.data.scenes:
                    if originalObject.name in scene.objects:
                        scene.objects.link(armBlObj).layers = layers

                if not isBone:
                    # our armature is named after the first non-bone upwards in the hierarchy.
                    # Also parent it to this object for simplicity.
                    armBlObj.parent = originalObject
                # else: the uppermost bone is a root object, so our armature won't need a parent at all.

                # position the armature in world-space. This is necessary so we can accurately position the bones.
                coord = armatureSOK.object.coord.object
                glMat = coord.localToWorld.glMat
                armBlObj.matrix_world = [glMat[:4], glMat[4:8], glMat[8:12], glMat[12:]]

                if not isBone:
                    # Parenting (which will be applied later) will screw up the armature's position.
                    # Keep track of that armature, we'll reset it's transform later.
                    willResetLocalTransformForArmatures.append(armBlObj)

                # register the armature
                armatureBones[armBlObj] = []

            # and register the bone as being part of this armature while we're at it
            armatureBones[armBlObj].append(sceneObjKey)
            self.boneToArmature[sceneObjKey] = armBlObj

        # now create the bones in each armature
        # (we iterate on each armature instead of each bone to avoid always toggling editmode, which is slow)
        for armatureKey, armBlObj in knownArmatures.items():
            arm = armBlObj.data
            # add a null bone to the armature.
            # (This bone doesn't exist in the Plasma scene, but has a matrix in the DSpan IIRC.
            # It's just supposed to represent world/ground position for trees. We'll fake it with a local bone, this usually works fine.)
            armBlObj.select = True
            bpy.context.scene.objects.active = armBlObj # the armature will be in the main scene at this point.
            bpy.ops.object.mode_set(mode='EDIT')
            blbone = arm.edit_bones.new("NULL")
            blbone.head = Vector((0, 0, 0))
            blbone.tail = Vector((0,0,self.bonesLength))
            blbone.matrix = Matrix()
            blbone["id"] = -1

            # while we're in editmode, create all other bones
            armBonesList = knownArmaturesDSpanAndBones[armBlObj][1]
            for sceneObjKey in armatureBones[armBlObj]:
                boneIds = self.bonesToReconstruct[sceneObjKey][1]
                # mmhkay, now let's add our bone.
                blbone = arm.edit_bones.new(sceneObjKey.name)
                blbone.head = (0,0,0)
                blbone.tail = (0,0,self.bonesLength)
                blbone["id"] = boneIds # add the bone's ids as a custom property. Just for debugging...
                boneNameToBoneKey[blbone.name] = sceneObjKey
                armBonesList.extend(boneIds) # this will be used to setup armature modifiers later

            # while we're STILL in editmode, correctly place all bones...
            armBones = arm.edit_bones
            for blBone in arm.edit_bones:
                if blBone.name == "NULL":
                    continue
                boneKey = boneNameToBoneKey[blBone.name]
                # let's try to position the bone correctly...
                boneSO = boneKey.object
                if not boneSO:
                    continue
                coordKey = boneSO.coord
                if not coordKey or not coordKey.object:
                    continue
                coord = coordKey.object
                glMat = coord.localToWorld.glMat
                blBone.matrix = [glMat[:4], glMat[4:8], glMat[8:12], glMat[12:]]
                blBone.matrix = armBlObj.matrix_world.inverted() * blBone.matrix
            # parent bones now that they are correctly placed
            for blBone in arm.edit_bones:
                if blBone.name == "NULL":
                    continue
                boneKey = boneNameToBoneKey[blBone.name]
                parentKey = self.parent.getParent(boneKey)
                if parentKey and parentKey.name.endswith("_REST"): # Korman compatibility
                    parentKey = self.parent.getParent(parentKey)
                if parentKey:
                    blBone.parent = armBones.get(parentKey.name)

            bpy.ops.object.mode_set(mode='OBJECT')
            armBlObj.select = False

            # now that we're OUT of editmode, and that our bones are all created...
            # some bones have more modifiers/interfaces (like light caster...) all parented to a "master" object (usually an empty)
            # parent this object to the bone itself
            for sceneObjKey in armatureBones[armBlObj]:
                master = self.parent.getBlObjectFromKey(sceneObjKey)
                master.parent = armBlObj
                master.parent_type = "BONE"
                master.parent_bone = sceneObjKey.name
                # ... and reset their transform (remove bonesLength to put it at the bone's head instead of tail)
                setLocalXForm(master, Vector((0, -self.bonesLength, 0)))

        # good. Now reset armatures' matrix_local (aka "local to parent"), so they snap in place later
        # once we reparent objects in the hierarchy
        for armBlObj in willResetLocalTransformForArmatures:
            armBlObj.matrix_local = Matrix.Identity(4)

        # and now that armatures exist, add armature modifiers...
        for blObj in self.objectsUsingBones:
            boneData = self.objectsUsingBones[blObj]
            dspanKey = boneData[0]
            baseBoneId = boneData[1]
            bonesLength = boneData[2]
            testBoneId = baseBoneId + 1
            armModifier = blObj.modifiers.new("Armature", "ARMATURE")
            for armBlObj in knownArmaturesDSpanAndBones:
                armDspan = knownArmaturesDSpanAndBones[armBlObj][0]
                armBones = knownArmaturesDSpanAndBones[armBlObj][1]
                if armDspan == dspanKey and (testBoneId in armBones):
                    armModifier.object = armBlObj

            # also add vertex groups for each bones
            blObj.vertex_groups.new("NULL") # mandatory "null" world bone
            for boneIndex in range(bonesLength):
                for futureBoneKey in self.bonesToReconstruct:
                    data = self.bonesToReconstruct[futureBoneKey]
                    futureBoneDSpan = data[0]
                    futureBoneIds = data[1]
                    if futureBoneDSpan == dspanKey:
                        # that's our span... and probably our armature too. Check if that's our bone
                        if boneIndex + baseBoneId in futureBoneIds:
                            # yup, all good.
                            blObj.vertex_groups.new(futureBoneKey.name)


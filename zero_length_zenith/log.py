# -*- coding:utf-8 -*-

"""
This file is part of ZLZ.

ZLZ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ZLZ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ZLZ.  If not, see <https://www.gnu.org/licenses/>
"""


"""
Used for logging to both the console and a log file.
#"""

import sys, traceback

class Log:
    class Output:
        def __init__(self, file, err):
            self.file = file
            self.err = err
            if self.err:
                self.handle = sys.stderr
                sys.stderr = self
            else:
                self.handle = sys.stdout
                sys.stdout = self

        def write(self, x):
            self.handle.write(x)
            self.file.write(x)

        def flush(self):
            self.handle.flush()
            self.file.flush()

        def close(self):
            if self.err:
                sys.stderr = self.handle
            else:
                sys.stdout = self.handle

    def __init__(self, filename):
        self.file=open(filename, "w")

    def __enter__(self):
        self.out = Log.Output(self.file, False)
        self.err = Log.Output(self.file, True)
        return self

    def __exit__(self, type, value, tb):
        if tb:
            self.file.write("Traceback (most recent call last):\n")
            traceback.print_tb(tb, file=self.file)
            self.file.write(type.__name__ + ": " + str(value) + "\n")
        self.out.close()
        self.err.close()
        self.file.close()

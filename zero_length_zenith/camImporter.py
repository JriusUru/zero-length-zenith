# -*- coding:utf-8 -*-

"""
This file is part of ZLZ.

ZLZ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ZLZ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ZLZ.  If not, see <https://www.gnu.org/licenses/>
"""


"""
Handles setting up cameras (both Blender and Korman's camera properties).
#"""

import bpy, bmesh
import PyHSPlasma as pl
from mathutils import *
from math import *
from zero_length_zenith.sceneImporter import SceneImporter
from zero_length_zenith.utils import *

class CamImporter:

    def __init__(self, parent):
        self.parent = parent
        self.camsToFinishSetup = []

    def importCam(self, sceneObjKey, camKey):
        if not self.parent.validKey(camKey):
            return None

        plCamMod = camKey.object
        if not plCamMod.brain or plCamMod.brain.type == pl.plFactory.kCameraBrain1:
            # this is a target point, not an actual camera. Hence, we don't give a fluff
            return None

        blCamera = bpy.data.cameras.new(camKey.name)
        blCamera.angle = plCamMod.fovW * pi / 180 # Blender cam is driven by width for landscape orientation
        blCamera.clip_start = .3 # should be Plasma's default if I'm not mistaken.
        blCamera.clip_end = bpy.context.scene.world.plasma_fni.yon # just copy it from the FNI file...
        blCamera.lens_unit = "FOV"

        blObj = bpy.data.objects.new(sceneObjKey.name + "_CAM", blCamera)
        self.parent.sceneImporter.appendObjectToScenes(blObj, SceneImporter.layerCameras)

        kCamera = blCamera.plasma_camera
        plBrain = plCamMod.brain.object
        kCamSettings = kCamera.settings

        if plCamMod.brain.type == pl.plFactory.kCameraBrain1_Circle:
            kCamera.camera_type = "circle"
        elif plCamMod.brain.type == pl.plFactory.kCameraBrain1_Avatar:
            kCamera.camera_type = "follow"
        elif plCamMod.brain.type == pl.plFactory.kCameraBrain1_Fixed:
            if plBrain.rail:
                kCamera.camera_type = "rail"
            else:
                kCamera.camera_type = "fixed"
        elif plCamMod.brain.type == pl.plFactory.kCameraBrain1_FirstPerson:
            kCamera.camera_type = "firstperson"

        if plBrain.getFlags(pl.plCameraBrain1.kFollowLocalAvatar):
            kCamSettings.poa_type = "avatar"
        elif plBrain.subject:
            kCamSettings.poa_type = "object"
            # (point on avatar object set later)
        else:
            kCamSettings.poa_type = "none"

        kCamSettings.poa_offset = Vector((plBrain.poaOffset.X, plBrain.poaOffset.Y, plBrain.poaOffset.Z))
        kCamSettings.poa_worldspace = plBrain.getFlags(pl.plCameraBrain1.kWorldspacePOA)
        kCamSettings.transition.poa_acceleration = plBrain.poaAcceleration
        kCamSettings.transition.poa_deceleration = plBrain.poaDeceleration
        kCamSettings.transition.poa_velocity = plBrain.poaVelocity
        kCamSettings.transition.poa_cut = plBrain.getFlags(pl.plCameraBrain1.kCutPOA)
        kCamSettings.transition.pos_acceleration = plBrain.acceleration
        kCamSettings.transition.pos_deceleration = plBrain.deceleration
        kCamSettings.transition.pos_velocity = plBrain.velocity
        kCamSettings.transition.pos_cut = plBrain.getFlags(pl.plCameraBrain1.kCutPos)
        if isinstance(plBrain, pl.plCameraBrain1_Avatar):
            kCamSettings.pos_offset = Vector((plBrain.offset.X, plBrain.offset.Y, plBrain.offset.Z))
        kCamSettings.x_pan_angle = plBrain.xPanLimit * 2
        kCamSettings.y_pan_angle = plBrain.zPanLimit * 2
        kCamSettings.pan_rate = plBrain.panSpeed
        kCamSettings.fov = radians(plCamMod.fovW)
        kCamSettings.limit_zoom = plBrain.getFlags(pl.plCameraBrain1.kZoomEnabled)
        kCamSettings.zoom_max = plBrain.zoomMax / (4 / 3)
        kCamSettings.zoom_min = plBrain.zoomMin / (4 / 3)
        kCamSettings.zoom_rate = plBrain.zoomRate
        if not isinstance(plBrain, pl.plCameraBrain1_FirstPerson):
            kCamSettings.maintain_los = plBrain.getFlags(pl.plCameraBrain1.kMaintainLOS)
            kCamSettings.fall_vertical = plBrain.getFlags(pl.plCameraBrain1.kVerticalWhenFalling)
            kCamSettings.fast_run = plBrain.getFlags(pl.plCameraBrain1.kSpeedUpWhenRunning)
        kCamSettings.ignore_subworld = plBrain.getFlags(pl.plCameraBrain1.kIgnoreSubworldMovement)

        if isinstance(plBrain, pl.plCameraBrain1_Circle):
            # (center object set later)
            kCamSettings.circle_pos = "farthest" if hasFlags(plBrain.circleFlags, pl.plCameraBrain1_Circle.kFarthest) else "closest"
            kCamSettings.circle_velocity = plBrain.cirPerSec * (2 * pi)
            kCamSettings.circle_radius_ui = plBrain.radius

        kCamSettings.anim_enabled = True # should already be the case, no reason not to enable
        kCamSettings.start_on_push = plCamMod.startAnimOnPush
        kCamSettings.stop_on_pop = plCamMod.stopAnimOnPop
        kCamSettings.reset_on_pop = plCamMod.resetAnimOnPop

        # (transitions set later)

        self.camsToFinishSetup.append((sceneObjKey, camKey, blObj))

        return blObj

    def setupReferences(self):
        # setup point on avatar, transitions and stuff
        for sceneObjKey, camKey, blObj in self.camsToFinishSetup:
            plCamMod = camKey.object
            blCamera = blObj.data
            kCamera = blCamera.plasma_camera
            plBrain = plCamMod.brain.object
            kCamSettings = kCamera.settings

            # kCamSettings.primary_camera = ? # not sure. Looks like it's only used by camera messages. Retrieving it is not worth the bother...

            if plBrain.subject:
                kCamSettings.poa_object = self.parent.getBlObjectFromKey(plBrain.subject)
                # conveniency, yo
                constraint = blObj.constraints.new("TRACK_TO")
                constraint.up_axis = "UP_Y"
                constraint.track_axis = "TRACK_NEGATIVE_Z"
                constraint.target = kCamSettings.poa_object

            if plCamMod.brain.type == pl.plFactory.kCameraBrain1_Circle:
                if plBrain.centerObject:
                    camCenter = self.parent.getBlObjectFromKey(plBrain.centerObject)
                else:
                    camCenter = bpy.data.objects.new(plCamMod.brain.name + "_CAM_CENTER", None)
                    camCenter.plasma_object.enabled = True
                    lyrs = [False] * 20
                    for lyrId in SceneImporter.layerCameras:
                        lyrs[lyrId] = True
                    for scene in bpy.data.scenes:
                        if blObj.name in scene.objects:
                            scene.objects.link(camCenter).layers = lyrs
                    setLocalXForm(camCenter, Vector((plBrain.center.X, plBrain.center.Y, plBrain.center.Z)))
                kCamSettings.circle_center = camCenter

            for plTrans in plCamMod.trans:
                if not plTrans.transTo:
                    # what's that ? Bah, probably some camera that got deleted before export.
                    continue
                ownerObj = None
                for sceneObjKey2, camKey2, blObj2 in self.camsToFinishSetup:
                    if camKey2 == plTrans.transTo:
                        ownerObj = blObj2
                kTrans = kCamera.transitions.add()
                kTrans.camera = ownerObj
                kTrans.mode = "manual"
                kTrans.enabled = True
                kTrans.poa_acceleration = plTrans.poaAccel
                kTrans.poa_deceleration = plTrans.poaDecel
                kTrans.poa_velocity = plTrans.poaVelocity
                kTrans.poa_cut = plTrans.cutPOA
                kTrans.pos_acceleration = plTrans.accel
                kTrans.pos_deceleration = plTrans.decel
                kTrans.pos_velocity = plTrans.velocity
                kTrans.pos_cut = plTrans.cutPos


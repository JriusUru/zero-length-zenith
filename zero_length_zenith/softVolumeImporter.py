# -*- coding:utf-8 -*-

"""
This file is part of ZLZ.

ZLZ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ZLZ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ZLZ.  If not, see <https://www.gnu.org/licenses/>
"""


"""
Handles setting up softvolumes and rebuilding their mesh.

This is a bit tricky because:
  - Softvolumes are stored as planes and not as meshes, making the rebuilding process harder.
  - Softvolumes don't always have a SceneObject owner.
  - Complex softvolumes rely on other softvolumes, which may not have been encountered yet.

Because of the last two, we will need to register each SV we come across, as well as callbacks
to assign the SV to whichever objects require it.
#"""

import bpy, bmesh
from queue import Queue
import PyHSPlasma as pl
from mathutils import *
from math import *
from zero_length_zenith.sceneImporter import SceneImporter
from zero_length_zenith.utils import *

class SoftVolumeImporter:

    def __init__(self, parent):
        self.parent = parent

        # { softVolumeKey, [callback, callback, ...] }
        # Will be executed once we are sure ALL softregions are imported.
        self.softVolumesCallbacks = {}
        # { visRegionKey, [callback, callback, ...] }
        # Will be executed once we are sure ALL visregions are imported.
        self.visRegionsCallbacks = {}

        self.softVolumesToImport = Queue()
        self.visRegionsToImport = Queue()

    def registerSoftVolumeAndCallback(self, softVolumeKey, callback):
        """Register the given soft volume/method pair. The softvolume will be imported later, and the callback executed once the object can be safely assigned to a Korman modifier, with the softvolume's Blender object as only parameter."""
        callbacks = self.softVolumesCallbacks.get(softVolumeKey)
        if callbacks:
            callbacks.append(callback)
        else:
            self.softVolumesCallbacks[softVolumeKey] = [callback];
            self.softVolumesToImport.put(softVolumeKey)

    def registerVisRegionAndCallback(self, visRegionKey, callback):
        """Register the given visregion/method pair. The visregion will be imported later, and the callback executed once the object can be safely assigned to a Korman modifier, with the visregion's Blender object as only parameter."""
        callbacks = self.visRegionsCallbacks.get(visRegionKey)
        if callbacks:
            callbacks.append(callback)
        else:
            self.visRegionsCallbacks[visRegionKey] = [callback];
            self.visRegionsToImport.put(visRegionKey)
        if self.parent.validKey(visRegionKey.object.region):
            # If the visreg is ownerless, we'll attach it to the SV it uses.
            # Register the SV so it gets imported first. We'll attach the VR to it later.
            self.softVolumesToImport.put(visRegionKey.object.region)

    def importSoftVolume(self, sceneObjKey, volumeKey):
        if not self.parent.validKey(volumeKey):
            return None

        if volumeKey.type == pl.plFactory.kSoftVolumeSimple:
            return self.importSoftVolumeSimple(sceneObjKey, volumeKey)
        elif volumeKey.type == pl.plFactory.kSoftVolumeInvert:
            return self.importSoftVolumeInvert(sceneObjKey, volumeKey)
        elif volumeKey.type == pl.plFactory.kSoftVolumeIntersect:
            return self.importSoftVolumeIntersect(sceneObjKey, volumeKey)
        elif volumeKey.type == pl.plFactory.kSoftVolumeUnion:
            return self.importSoftVolumeUnion(sceneObjKey, volumeKey)
        else:
            raise TypeError("Unexpected soft volume type %d" % volumeKey.type)

    def importSoftVolumeSimple(self, sceneObjKey, volumeKey):
        plVolumeMod = volumeKey.object
        hasOwnerObject = bool(sceneObjKey)
        objName = sceneObjKey.name + "_SOFTVOLUME" if hasOwnerObject else volumeKey.name

        mesh = bpy.data.meshes.new(volumeKey.name)
        blObj = bpy.data.objects.new(objName, mesh)

        if hasOwnerObject:
            self.parent.sceneImporter.appendObjectToScenes(blObj, SceneImporter.layerSoftVolumes)
        else:
            # since objectImporter does not handle this object, we have to register it ourself and put it in the correct scene...
            self.parent.sceneImporter.appendObjectToScenesWithPlLocation(blObj, SceneImporter.layerSoftVolumes, volumeKey.location)
            self.parent.registerObject(volumeKey, blObj)

        blObj.draw_type = "WIRE"
        blObj.hide_render = True

        kormanMod = self.createKormanSoftVolumeMod(blObj)
        kormanMod.use_nodes = False
        kormanMod.invert = False
        kormanMod.inside_strength = plVolumeMod.insideStrength * 100
        kormanMod.outside_strength = plVolumeMod.outsideStrength * 100
        kormanMod.soft_distance = plVolumeMod.softDist

        if plVolumeMod.volume is None:
            # Life is just that weird sometimes.
            print("WARNING - softvolume without a volume. Importing as empty mesh...")
            return

        # rebuild the mesh.
        # Ideally we would compute the vertices at the intersection of each of the SV's planes.
        # But I don't really know how to do that.
        # So instead let's use a correct but ugly workaround: place one quad where each plane is supposed to be.
        # This will be correct enough that Korman accepts reexporting it, and this way I don't have to spend all night on it.

        # first, compute the bounding volume of the SV.
        # This has no real use, it's just so we can scale our floating faces a bit better.
        lowerBound = [inf, inf, inf]
        upperBound = [-inf, -inf, -inf]
        for plane in plVolumeMod.volume.planes:
            if lowerBound[0] > plane.pos.X: lowerBound[0] = plane.pos.X
            if lowerBound[1] > plane.pos.Y: lowerBound[1] = plane.pos.Y
            if lowerBound[2] > plane.pos.Z: lowerBound[2] = plane.pos.Z
            if upperBound[0] < plane.pos.X: upperBound[0] = plane.pos.X
            if upperBound[1] < plane.pos.Y: upperBound[1] = plane.pos.Y
            if upperBound[2] < plane.pos.Z: upperBound[2] = plane.pos.Z
        diagonal = (upperBound[0] - lowerBound[0],
                    upperBound[1] - lowerBound[1],
                    upperBound[2] - lowerBound[2])
        bboxCenter = Vector(((upperBound[0] + lowerBound[0]) / 2,
                             (upperBound[1] + lowerBound[1]) / 2,
                             (upperBound[2] + lowerBound[2]) / 2))
        volumeApproxSize = sqrt(diagonal[0] ** 2 + diagonal[1] ** 2 + diagonal[2] ** 2)

        if volumeApproxSize > 0 and len(plVolumeMod.volume.planes) >= 2:
            # the more planes we have in our SV, the smaller each of our floating face get.
            # The following formula is random but should yield fairly acceptable results.
            faceSizeMultiplier = volumeApproxSize / (len(plVolumeMod.volume.planes) * 2)
        else:
            faceSizeMultiplier = 1

        bm = bmesh.new()
        assert(isinstance(plVolumeMod.volume, pl.plConvexIsect))

        # create 4 vertices for each plane
        for plane in plVolumeMod.volume.planes:
            pos = Vector((plane.pos.X, plane.pos.Y, plane.pos.Z))
            norm = Vector((plane.norm.X, plane.norm.Y, plane.norm.Z))
            sideA = norm.cross(Vector((0, 0, 1))).normalized()
            if sideA.length < .01:
                sideA = Vector((1, 0, 0))
            sideB = norm.cross(sideA).normalized()
            # now create a quad
            # oh, I forgot to mention something. plane.pos is incorrect. It's on the correct plane, sure, but it uses
            # the position of one of the original vertices instead of the plane's center. So we have to compute that too.
            # Sigh... Just align the normal with the bounding box center...
            newPos = bboxCenter + (pos - bboxCenter).project(norm)
            pos0 = newPos + sideA * faceSizeMultiplier
            pos1 = newPos + sideB * faceSizeMultiplier
            pos2 = newPos - sideA * faceSizeMultiplier
            pos3 = newPos - sideB * faceSizeMultiplier
            bm.verts.new((pos0.x, pos0.y, pos0.z))
            bm.verts.new((pos1.x, pos1.y, pos1.z))
            bm.verts.new((pos2.x, pos2.y, pos2.z))
            bm.verts.new((pos3.x, pos3.y, pos3.z))

        bm.verts.ensure_lookup_table()

        # now make faces for each vert quatuor
        for i in range(len(plVolumeMod.volume.planes)):
            bm.faces.new(bm.verts[i * 4 : i * 4 + 4])

        bm.to_mesh(mesh)
        bm.free()
        del bm

        return blObj

    def importSoftVolumeInvert(self, sceneObjKey, volumeKey):
        # Note: we NEVER import inverted SV as a Korman "simple" SV with the "invert" checkbox on.
        # The reason for this is we don't know if any other object uses the base, non inverted SV...
        hasOwnerObject = bool(sceneObjKey)
        objName = sceneObjKey.name + "_SOFTVOLUME" if hasOwnerObject else volumeKey.name
        blObj = bpy.data.objects.new(objName, None)

        if hasOwnerObject:
            self.parent.sceneImporter.appendObjectToScenes(blObj, SceneImporter.layerSoftVolumes)
        else:
            # since objectImporter does not handle this object, we have to register it ourself and put it in the correct scene...
            self.parent.sceneImporter.appendObjectToScenesWithPlLocation(blObj, SceneImporter.layerSoftVolumes, volumeKey.location)
            self.parent.registerObject(volumeKey, blObj)

        plVolumeMod = volumeKey.object

        kormanMod = self.createKormanSoftVolumeMod(blObj)
        kormanMod.use_nodes = True
        tree = bpy.data.node_groups.new(volumeKey.name, "PlasmaNodeTree")
        kormanMod.node_tree = tree
        node_output = tree.nodes.new("PlasmaSoftVolumeOutputNode")
        node_invert = tree.nodes.new("PlasmaSoftVolumeInvertNode")
        node_input = tree.nodes.new("PlasmaSoftVolumeReferenceNode")

        node_output.link_input(node_invert, node_invert.outputs[0], node_output.inputs[0])
        node_invert.link_input(node_input, node_input.outputs[0], node_invert.inputs[1])

        def setSubvolumeObject(subVolumeBlObject):
            node_input.soft_volume = subVolumeBlObject
        self.registerSoftVolumeAndCallback(plVolumeMod.subVolumes[0], setSubvolumeObject)

        # position the nodes (seriously, Blender can't fucking do it itself ? Grmblrm)
        node_output.location = (0, 0)
        node_invert.location = (-200, 0)
        node_input.width *= 2
        node_input.location = (-560, 0)

        if plVolumeMod.insideStrength != 1 and plVolumeMod.outsideStrength != 0:
            node_properties = tree.nodes.new("PlasmaSoftVolumePropertiesNode")
            node_properties.inside_strength = plVolumeMod.insideStrength * 100
            node_properties.outside_strength = plVolumeMod.outsideStrength * 100
            node_properties.location = (-400, 200)
            node_invert.link_input(node_properties, node_properties.outputs[0], node_invert.inputs[0])

        return blObj

    def importSoftVolumeIntersect(self, sceneObjKey, volumeKey):
        hasOwnerObject = bool(sceneObjKey)
        objName = sceneObjKey.name + "_SOFTVOLUME" if hasOwnerObject else volumeKey.name
        blObj = bpy.data.objects.new(objName, None)

        if hasOwnerObject:
            self.parent.sceneImporter.appendObjectToScenes(blObj, SceneImporter.layerSoftVolumes)
        else:
            # since objectImporter does not handle this object, we have to register it ourself and put it in the correct scene...
            self.parent.sceneImporter.appendObjectToScenesWithPlLocation(blObj, SceneImporter.layerSoftVolumes, volumeKey.location)
            self.parent.registerObject(volumeKey, blObj)

        plVolumeMod = volumeKey.object

        kormanMod = self.createKormanSoftVolumeMod(blObj)
        kormanMod.use_nodes = True
        tree = bpy.data.node_groups.new(volumeKey.name, "PlasmaNodeTree")
        kormanMod.node_tree = tree
        node_output = tree.nodes.new("PlasmaSoftVolumeOutputNode")
        node_intersect = tree.nodes.new("PlasmaSoftVolumeIntersectNode")

        node_output.link_input(node_intersect, node_intersect.outputs[0], node_output.inputs[0])

        def setSubvolumeObject(node, subVolumeBlObject):
            node.soft_volume = subVolumeBlObject
        for subVolumeId, subVolumeKey in enumerate(plVolumeMod.subVolumes):
            if not self.parent.validKey(subVolumeKey):
                continue
            node_input = tree.nodes.new("PlasmaSoftVolumeReferenceNode")
            node_intersect.link_input(node_input, node_input.outputs[0], node_intersect.inputs[subVolumeId + 1])
            node_input.width *= 2
            node_input.location = (-560, -100 * subVolumeId)
            self.registerSoftVolumeAndCallback(subVolumeKey, lambda subVolumeBlObject, node=node_input: setSubvolumeObject(node, subVolumeBlObject))

        # position the nodes (seriously, Blender can't fucking do it itself ? Grmblrm)
        node_output.location = (0, 0)
        node_intersect.location = (-200, 0)

        if plVolumeMod.insideStrength != 1 and plVolumeMod.outsideStrength != 0:
            node_properties = tree.nodes.new("PlasmaSoftVolumePropertiesNode")
            node_properties.inside_strength = plVolumeMod.insideStrength * 100
            node_properties.outside_strength = plVolumeMod.outsideStrength * 100
            node_properties.location = (-400, 200)
            node_intersect.link_input(node_properties, node_properties.outputs[0], node_intersect.inputs[0])

        return blObj

    def importSoftVolumeUnion(self, sceneObjKey, volumeKey):
        hasOwnerObject = bool(sceneObjKey)
        objName = sceneObjKey.name + "_SOFTVOLUME" if hasOwnerObject else volumeKey.name
        blObj = bpy.data.objects.new(objName, None)

        if hasOwnerObject:
            self.parent.sceneImporter.appendObjectToScenes(blObj, SceneImporter.layerSoftVolumes)
        else:
            # since objectImporter does not handle this object, we have to register it ourself and put it in the correct scene...
            self.parent.sceneImporter.appendObjectToScenesWithPlLocation(blObj, SceneImporter.layerSoftVolumes, volumeKey.location)
            self.parent.registerObject(volumeKey, blObj)

        plVolumeMod = volumeKey.object

        kormanMod = self.createKormanSoftVolumeMod(blObj)
        kormanMod.use_nodes = True
        tree = bpy.data.node_groups.new(volumeKey.name, "PlasmaNodeTree")
        kormanMod.node_tree = tree
        node_output = tree.nodes.new("PlasmaSoftVolumeOutputNode")
        node_union = tree.nodes.new("PlasmaSoftVolumeUnionNode")

        node_output.link_input(node_union, node_union.outputs[0], node_output.inputs[0])

        def setSubvolumeObject(node, subVolumeBlObject):
            node.soft_volume = subVolumeBlObject
        for subVolumeId, subVolumeKey in enumerate(plVolumeMod.subVolumes):
            if not self.parent.validKey(subVolumeKey):
                continue
            node_input = tree.nodes.new("PlasmaSoftVolumeReferenceNode")
            node_union.link_input(node_input, node_input.outputs[0], node_union.inputs[subVolumeId + 1])
            node_input.width *= 2
            node_input.location = (-560, -100 * subVolumeId)
            self.registerSoftVolumeAndCallback(subVolumeKey, lambda subVolumeBlObject, node=node_input: setSubvolumeObject(node, subVolumeBlObject))

        # position the nodes (seriously, Blender can't fucking do it itself ? Grmblrm)
        node_output.location = (0, 0)
        node_union.location = (-200, 0)

        if plVolumeMod.insideStrength != 1 and plVolumeMod.outsideStrength != 0:
            node_properties = tree.nodes.new("PlasmaSoftVolumePropertiesNode")
            node_properties.inside_strength = plVolumeMod.insideStrength * 100
            node_properties.outside_strength = plVolumeMod.outsideStrength * 100
            node_properties.location = (-400, 200)
            node_union.link_input(node_properties, node_properties.outputs[0], node_union.inputs[0])

        return blObj

    def createKormanSoftVolumeMod(self, blObj):
        kormanMod = blObj.plasma_modifiers.softvolume
        kormanMod.enabled = True
        return kormanMod

    def importVisRegion(self, blObj, visRegionKey):
        if not self.parent.validKey(visRegionKey):
            return None

        plVisRegion = visRegionKey.object

        if not blObj:
            # "floaty" visregion without owner
            # since objectImporter does not handle this object, we have to register it ourself and put it in the correct scene...
            if self.parent.validKey(plVisRegion.region):
                # just put it on the same object as the softvolume for simplicity...
                blObj = self.parent.getBlObjectFromKey(plVisRegion.region)
            else:
                blObj = bpy.data.objects.new(visRegionKey.name, None)
                self.parent.sceneImporter.appendObjectToScenesWithPlLocation(blObj, SceneImporter.layerSoftVolumes, visRegionKey.location)
            self.parent.registerObject(visRegionKey, blObj)

        kormanMod = blObj.plasma_modifiers.visregion
        kormanMod.enabled = True

        if plVisRegion.getProperty(pl.plVisRegion.kDisable):
            kormanMod.mode = "fx"
        else:
            kormanMod.mode = "exclude" if plVisRegion.getProperty(pl.plVisRegion.kIsNot) else "normal"

        if self.parent.validKey(plVisRegion.region):
            softRegKey = plVisRegion.region
            def setSoftVolume(softVolumeObject):
                kormanMod.soft_region = softVolumeObject
            self.registerSoftVolumeAndCallback(softRegKey, setSoftVolume)

        kormanMod.replace_normal = plVisRegion.getProperty(pl.plVisRegion.kReplaceNormal)

        return kormanMod

    def importOwnerlessSoftvolumes(self):
        """Create Blender objects for all softvolumes/visregions that haven't been imported yet."""
        while not self.softVolumesToImport.empty():
            softVolumeKey = self.softVolumesToImport.get()
            if self.parent.validKey(softVolumeKey):
                if not self.parent.isPlObjectImported(softVolumeKey):
                    self.importSoftVolume(None, softVolumeKey)

        while not self.visRegionsToImport.empty():
            visRegionKey = self.visRegionsToImport.get()
            if self.parent.validKey(visRegionKey):
                if not self.parent.isPlObjectImported(visRegionKey):
                    self.importVisRegion(None, visRegionKey)

    def setupReferences(self):
        for visRegionKey, callbacks in self.visRegionsCallbacks.items():
            for callback in callbacks:
                callback(self.parent.getBlObjectFromKey(visRegionKey));

        for softVolumeKey, callbacks in self.softVolumesCallbacks.items():
            for callback in callbacks:
                callback(self.parent.getBlObjectFromKey(softVolumeKey));

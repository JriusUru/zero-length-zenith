# The ZLZ importer !


The ZLZ importer can import Uru Ages (`.age` and `.prp`) into Blender, allowing you to study how Cyan built their Ages.

## Requirements

ZLZ requires Blender (preferably version 2.79), with the [Korman](https://github.com/H-uru/korman) plugin installed with it.

## Installation

Download the ZLZ package.

There are two ways to install it. The easiest is through the "Install Add-on from File" button in Blender's addons window. Just select the ZIP file you downloaded, and activate it. Alternatively, if for some reason you want to install it manually, you can extract the `zero_length_zenith` folder to `<Blender's location>/<version number>/scripts/addons`.

In both cases, don't forget to activate it by clicking its checkbox (and preferably click the "Save User Settings" button after that).

## Why this name in particular ?

It's a reference to Mystcraft, which I was playing when I started developing this plugin. And no, I'm not apologizing for it.